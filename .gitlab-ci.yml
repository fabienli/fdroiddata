image: registry.gitlab.com/fdroid/ci-images-server:latest

lint:
  except:
    - triggers
  before_script:
    - ./tools/trigger-issuebot
    - printf "Package\x3a androguard fdroidserver python3-asn1crypto python3-ruamel.yaml\nPin\x3a release a=stretch-backports\nPin-Priority\x3a 500\n" > /etc/apt/preferences.d/debian-stretch-backports.pref
    - echo "deb http://deb.debian.org/debian/ stretch-backports main" > /etc/apt/sources.list.d/backports.list
    - apt-get update
    - apt-get -qy dist-upgrade

    - rm -rf fdroidserver
    - mkdir fdroidserver
    - curl --silent https://gitlab.com/fdroid/fdroidserver/repository/master/archive.tar.gz
        | tar -xz --directory=fdroidserver --strip-components=1
    - export PATH="$PWD/fdroidserver:$PATH"
    - touch config.py
  script:
    # if this is a merge request fork, then only check relevant apps
    - if [ "$CI_PROJECT_NAMESPACE" != "fdroid" ]; then
        git fetch https://gitlab.com/fdroid/fdroiddata;
        test -d build || mkdir build;
        for f in `git diff --name-only --diff-filter=d FETCH_HEAD...HEAD`; do
           appid=`echo $f | sed -n -e 's,^metadata/\([^/][^/]*\)\.yml,\1,p'`;
           export CHANGED="$CHANGED $appid";
           grep -q "^Repo *Type\W *git$" $f && tail -1 $f | grep -qv '^NoSourceSince' && git -C build clone `sed -n "s,^Repo *:,,p" $f` $appid;
        done;
        ./tools/audit-gradle.py $CHANGED;
      fi
    - export EXITVALUE=0
    - fdroid lint -f $CHANGED || {
          export EXITVALUE=1;
          printf "\nThese files have lint issues:\n";
          fdroid rewritemeta -l $CHANGED;
          printf "\nThese are the formatting issues:\n";
          fdroid rewritemeta $CHANGED;
          git --no-pager diff --color=always;
      }
    - apt-get -qy update
    - apt-get -qy install --no-install-recommends exiftool
    - find metadata/ -name '*.jp*g' -o -name '*.png' | xargs exiftool -all=
    - echo "these images have EXIF that must be stripped:"
    - git --no-pager diff --stat
    - git --no-pager diff --name-only --exit-code || export EXITVALUE=1
    - ./tools/check-localized-metadata.py || export EXITVALUE=1
    - ./tools/check-keyalias-collision.py || export EXITVALUE=1
    - ./tools/check-metadata-summary-whitespace.py || export EXITVALUE=1
    - exit $EXITVALUE


pages:
  image: registry.gitlab.com/fdroid/ci-images-client:latest
  only:
    - triggers
    - web
  artifacts:
    paths:
      - metadata/
      - public/
      - repo/index-v1.json
      - repo/index.xml
      - tmp/apkcache.json
    when: always
  script:
    - apt-get update
    - apt-get dist-upgrade
    # install fdroidserver from git, with deps from Debian, until fdroidserver
    # is stable enough to include all the things needed here
    - apt-get install -t stretch-backports
         fdroidserver
         python3-asn1crypto
         python3-ruamel.yaml
         python3-venv
    - apt-get purge fdroidserver
    - test -d fdroidserver || mkdir fdroidserver
    - curl --silent https://gitlab.com/fdroid/fdroidserver/repository/master/archive.tar.gz
        | tar -xz --directory=fdroidserver --strip-components=1
    - export PATH="`pwd`/fdroidserver:$PATH"
    - export PYTHONPATH="`pwd`/fdroidserver"
    - export PYTHONUNBUFFERED=true

    - export GRADLE_USER_HOME=$PWD/.gradle
    - rm -rf $GRADLE_USER_HOME/fdroid
    - mkdir -p $GRADLE_USER_HOME/fdroid
    - curl --silent https://gitlab.com/fdroid/gradle-plugins/repository/master/archive.tar.gz
        | tar -xz --directory=$GRADLE_USER_HOME/fdroid --strip-components=1

    - curl --silent https://gitlab.com/fdroid/issuebot/repository/master/archive.tar.gz
        | tar -xz --strip-components=1
    - pyvenv --system-site-packages --clear issuebot-env
    - . issuebot-env/bin/activate
    - pip3 install python-gitlab wheel pygithub
    - ./issuebot.py

    # git_stats used to run here, redirect to new location
    - echo '<html><head><meta http-equiv="refresh" content="0;URL=https://fdroid.gitlab.io/"></head></html>'
        > public/index.html


check_git_repos:
  image: debian:buster-slim
  stage: test
  only:
    - schedules
  artifacts:
    when: on_failure
    expire_in: 1 month
    paths:
      - public
  script:
    - apt-get update
    - apt-get -qy install --no-install-recommends ca-certificates git python3-yaml
    - tools/check-git-repo-availability.py || export EXITVALUE=1
    - test -d public || mkdir public
    - cp `git status | grep -Eo 'metadata/.*\.yml'` public/ || true
    - exit $EXITVALUE

fdroid-buildserver:
  tags:
    - fdroid
    - buildserver
  only:
     - DONOTRUN
#    - branches@beuc/fdroiddata
#    - branches@Bubu/fdroiddata
#    - branches@eighthave/fdroiddata
#    - branches@fdroid/fdroiddata
#    - branches@grote/fdroiddata
#    - branches@izzysoft/fdroiddata
#    - branches@schildbach/fdroiddata
#    - branches@uniqx/fdroiddata
  artifacts:
    name: "${CI_PROJECT_PATH}_${CI_JOB_STAGE}_${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}"
    paths:
      - json
      - libscout-logs
      - logs
      - stats
      - unsigned
    expire_in: 1 week
    when: always
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - build
      - unsigned
  script:
    - virsh -c qemu:///system destroy builder_default || true
    - virsh -c qemu:///system undefine builder_default
          --nvram --managed-save --remove-all-storage --snapshots-metadata || true
    - git -C $HOME/fdroidserver reset --hard
    - git -C $HOME/fdroidserver checkout -B master origin/master
    - export PATH="$HOME/fdroidserver:$PATH"
    - if [ "$CI_PROJECT_NAMESPACE" = "fdroid" ]; then
          export head=HEAD^^^;
          echo ORIG_HEAD `git diff --name-only --diff-filter=d ORIG_HEAD...HEAD
             | sed -n -e 's,^metadata/\([^/][^/]*\)\.txt,\1,p' -e 's,^metadata/\([^/][^/]*\)\.yml,\1,p'`;
      else
          git fetch https://gitlab.com/fdroid/fdroiddata.git;
          export head=FETCH_HEAD;
      fi
    - export appids=`git diff --name-only --diff-filter=d $head...HEAD
         | sed -n -e 's,^metadata/\([^/][^/]*\)\.txt,\1,p' -e 's,^metadata/\([^/][^/]*\)\.yml,\1,p'`
    - echo $appids | grep '\w' || exit 0
    - fdroid build --verbose --server --stop --no-tarball --latest $appids
    - find unsigned -maxdepth 0 -empty -exec exit 0 \;
    - cd ~/libscout
    - java -jar build/LibScout.jar -o match -a lib/android-23.jar -p profiles
           -j $CI_PROJECT_DIR/json
           -s $CI_PROJECT_DIR/stats
           -d $CI_PROJECT_DIR/libscout-logs
           $CI_PROJECT_DIR/unsigned
    - for f in $CI_PROJECT_DIR/libscout-logs/*.log; do printf "\n\n$f\n"; cut -b43-160 $f; done
    - for f in `find $CI_PROJECT_DIR/json -name \*.json`; do cat $f | python3 -m json.tool; done
  after_script:
    - test -d builder || exit 0
    - cd builder
    - vagrant destroy --force
